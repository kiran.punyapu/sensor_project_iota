///////////////////////////////
// Fetch your HELLOWORLD Message
///////////////////////////////

const iotaLibrary = require('@iota/core')
const Converter = require('@iota/converter')

const iota = iotaLibrary.composeAPI({
  provider: 'http://etodevice:EtoGruppe@26062020@88.79.243.113:14265'
})
const Extract = require('@iota/extract-json')

const address =
  'WIXFPIEXZBQSZOEUNRIEZW9LCSOMDMBCBTVTNSDFTSUPNGUH9EFSIGXOHVQGXNKHZYPOVQDKLNENXWOLZ'

iota
  .findTransactionObjects({ addresses: [address] })
  .then(response => {
    console.log(response)
    const msg = response.sort((a, b) => a.currentIndex - b.currentIndex).map(tx => tx.signatureMessageFragment).join('')
    console.log('Encoded message:')
    console.log(msg)
    var trytes = Converter.asciiToTrytes(msg);
    console.log(trytes)
    //Convert trytes to plan text
    const data = Converter.trytesToAscii(trytes)
    console.log('Decoded message:')
    console.log(data)
  })
  .catch(err => {
    console.error(err)
  })
