///////////////////////////////
// MAM: Fetch messages from Public Stream using devnet provider
///////////////////////////////

const Mam = require('@iota/mam')
const { trytesToAscii } = require('@iota/converter')

// Initialize MAM State - PUBLIC
Mam.init('https://nodes.devnet.iota.org:443')

const root =
'JOTP9NVEDYVQVRHHJTAMYYFZIKJGJRZKRBNXXDDNVHFOBLEB9BEQQOUQA9MUPZRNEIYOBPGK9DYJRUKHT'

// Display coordinate data on our screen when we receive it
function showData(raw) {
  const data = trytesToAscii(raw)
  console.log(data)
}

const readMam = async () => {
  const actdata=await Mam.fetch(root, 'public', null, showData);
}

readMam()



// public send and fetch using devnet
// const Mam = require('@iota/mam')
// const { asciiToTrytes, trytesToAscii } = require('@iota/converter')

// const mode = 'public'
// const provider = 'https://nodes.devnet.iota.org:443'

// const mamExplorerLink = `https://mam-explorer.firebaseapp.com/?provider=${encodeURIComponent(provider)}&mode=${mode}&root=`

// // Initialise MAM State
// let mamState = Mam.init(provider)

// // Publish to tangle
// const publish = async packet => {
//     // Create MAM Payload - STRING OF TRYTES
//     const trytes = asciiToTrytes(JSON.stringify(packet))
//     const message = Mam.create(mamState, trytes)

//     // Save new mamState
//     mamState = message.state

//     // Attach the payload
//     await Mam.attach(message.payload, message.address, 3, 9)

//     console.log('Published', packet, '\n');
//     return message.root
// }

// const publishAll = async () => {
//   const root = await publish({
//     message: {
//       "sensortype":"UV sensor",
// "data":[
//   20,
//   30,
//   40,
//   50,
//   60,
//   70
// ],
//     },
//     timestamp: (new Date()).toLocaleString()
//   })

//   await publish({
//     message: {
//       "sensortype":" sensor",
// "data":[
//   20,
//   30,
//   40,
//   50,
//   60,
//   70
// ],
//     },
//     timestamp: (new Date()).toLocaleString()
//   })

//   await publish({
//     message: {
//       "sensortype":"Humidity sensor",
// "data":[
//   20,
//   30,
//   40,
//   50,
//   60,
//   70
// ],
//     },
//     timestamp: (new Date()).toLocaleString()
//   })

//   return root
// }



// // Callback used to pass data out of the fetch
// const logData = data => console.log('Fetched and parsed', JSON.parse(trytesToAscii(data)), '\n')

// publishAll()
//   .then(async root => {

//     // Output asyncronously using "logData" callback function
//     await Mam.fetch(root, mode, null, logData)

//     // Output syncronously once fetch is completed
//     const result = await Mam.fetch(root, mode)
//     result.messages.forEach(message => console.log('Fetched and parsed', JSON.parse(trytesToAscii(message)), '\n'))

//     console.log(`Verify with MAM Explorer:\n${mamExplorerLink}${root}\n`);
//   })
