///////////////////////////////
// MAM: Fetching messages from restricted Stream
///////////////////////////////

const Mam = require('@iota/mam')
const { trytesToAscii } = require('@iota/converter')

// Init State
let root =
  '9NCNYVSQXNUQXSTFBMJYSNMSVPXKENHZNAAXLYG99KXXTMIKNXOQFKLWDCOTBZGQSDOS9OEA9NMEHGPYI'
const mamType = 'restricted'
const mamSecret = 'SENSORAPP'

// Initialise MAM State
let mamState = Mam.init('http://etodevice:EtoGruppe@26062020@88.79.243.113:14265')

// Callback used to pass data out of the fetch
const logData = data => console.log(trytesToAscii(data))

const execute = async () => {
  // Callback used to pass data + returns next_root
  const resp = await Mam.fetch(root, mamType, mamSecret, logData)
}
execute()
