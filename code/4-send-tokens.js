///////////////////////////////
// Send tokens
///////////////////////////////

const iotaLibrary = require('@iota/core')
const Converter = require('@iota/converter');

const iota = iotaLibrary.composeAPI({
  provider: 'https://nodes.comnet.thetangle.org:443'
})

// Replace this with the seed you sent tokens too!
const seed =
  'PUEOTSEITFEVEWCWBTSIZM9NKRGJEIMXTULBACGFRQK9IMGICLBKW9TTEVSDQMGWKBXPVCBMMCXWMNPDX'

// Create a wrapping function so we can use async/await
const main = async () => {
  // Generate a different address from your seed
  const receivingAddress = await iota.getNewAddress(seed, {
    index: 1,
    total: 1
  })

  // Construct a TX to our new address
  const address = receivingAddress[0]
  const message = JSON.stringify({"message": "kiran"});
const messageInTrytes = Converter.asciiToTrytes(message);

  const transfers = [
    {
        value: 500,
        address: address,
        message: messageInTrytes
    }
    ];
  console.log('Sending 500i to ' + receivingAddress)
  console.log('Sending 500i to ' + JSON.stringify(transfers))

  try {
    // Construct bundle and convert to trytes
    const trytes = await iota.prepareTransfers(seed, transfers)
    // Send bundle to node.
    const response = await iota.sendTrytes(trytes, 3, 10)

    console.log('Completed TXs')
    response.map(tx => console.log(tx))
  } catch (e) {
    console.log(e)
  }
}

main()
