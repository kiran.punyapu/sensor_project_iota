///////////////////////////////
// Create an address from a new seed
/////
// First: run this code in a unix based terminal to generate an 81 Tryte seed.
// 'cat /dev/urandom |LC_ALL=C tr -dc 'A-Z9' | fold -w 81 | head -n 1'
// Paste the output of the above code into the 'seed' section below.
///////////////////////////////

const iotaLibrary = require('@iota/core')
const Checksum = require('@iota/checksum')

const iota = iotaLibrary.composeAPI({
  provider: 'http://etodevice:EtoGruppe@26062020@88.79.243.113:14265'
})

const seed =
  'QPQEYBOMQZUFZSCXNPSPGGBMGGXRFAQ9WESGDZSDSLOH9TBBZIIEOYJUKTMTAJZIPBWFWCRWUHGFIAWWT'  

iota
  .getNewAddress(seed, { index: 0})
  .then(address => {
    console.log(address)
    // const addressWithChecksum = Checksum.addChecksum(address);
    // console.log(addressWithChecksum)
    // console.log('valid: ' +  Checksum.isValidChecksum('UDYXTZBE9GZGPM9SSQV9LTZNDLJIZMPUVVXYXFYVBLIEUHLSEWFTKZZLXYRHHWVQV9MNNX9KZC9D9UZWZ'))
    // console.log('Paste this address into https://faucet.comnet.einfachiota.de/')
  })
  .catch(err => {
    console.log(err)
  })
