// ///////////////////////////////
// // MAM: Publish messages to Private Stream
// ///////////////////////////////

// const Mam = require('@iota/mam')
// const { asciiToTrytes } = require('@iota/converter')

// let mamState = Mam.init('https://nodes.comnet.thetangle.org:443')

// // We are using MAM restricted mode with a shared secret in this example
// const mamType = 'restricted'
// const mamSecret = 'DONTSHARETHIS'

// mamState = Mam.changeMode(mamState, mamType, mamSecret)

// const publish = async data => {
//   // Convert the JSON to trytes and create a MAM message
//   const trytes = asciiToTrytes(data)
//   const message = Mam.create(mamState, trytes)

//   // Update the MAM state to the state of this latest message
//   mamState = message.state

//   // Attach the message
//   await Mam.attach(message.payload, message.address, 3, 10)
//   console.log('Sent message to the Tangle!')
//   console.log('Address: ' + message.root)
// }

// publish('Super Secret Message')
// publish('Super Secret Message2')



// MAM: Publish and fetching messages from restricted Stream

const Mam = require('@iota/mam')
const { asciiToTrytes, trytesToAscii } = require('@iota/converter')

const mode = 'restricted'
const secretKey = 'SENSORAPP'
const provider = 'http://etodevice:EtoGruppe@26062020@88.79.243.113:14265'

const mamExplorerLink = `https://mam-explorer.firebaseapp.com/?provider=${encodeURIComponent(provider)}&mode=${mode}&key=${secretKey.padEnd(81, '9')}&root=`

// Initialise MAM State
let mamState = Mam.init(provider)

// Set channel mode
mamState = Mam.changeMode(mamState, mode, secretKey)

// Publish to tangle
const publish = async packet => {
    // Create MAM Payload - STRING OF TRYTES
    const trytes = asciiToTrytes(JSON.stringify(packet))
    const message = Mam.create(mamState, trytes)

    // Save new mamState
    mamState = message.state

    // Attach the payload
    await Mam.attach(message.payload, message.address, 3, 14)

    console.log('Published', packet, '\n');
    return message.root
}

const publishAll = async () => {
  const root = await publish({
      message: {
            "sensortype":"UV sensor",
      "data":[
        20,
        30
      ],
          },
    timestamp: (new Date()).toLocaleString()
  })

  await publish({
    message: {
      "sensortype":"temp sensor",
"data":[
  20,
  30
],
    },
timestamp: (new Date()).toLocaleString()
  })

  await publish({
    message: {
      "sensortype":"humid sensor",
"data":[
  20,
  30
],
    },
timestamp: (new Date()).toLocaleString()
  })

  return root
}

// Callback used to pass data out of the fetch
const logData = data => console.log('Fetched and parsed', JSON.parse(trytesToAscii(data)), '\n')

publishAll()
  .then(async root => {

    // Output asyncronously using "logData" callback function
    await Mam.fetch(root, mode, secretKey, logData)

    // Output syncronously once fetch is completed
    const result = await Mam.fetch(root, mode, secretKey)
    result.messages.forEach(message => console.log('Fetched and parsed', JSON.parse(trytesToAscii(message)), '\n'))

    console.log(`Verify with MAM Explorer:\n${mamExplorerLink}${root}\n`);
  })
