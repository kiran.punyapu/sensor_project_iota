///////////////////////////////
// Send HELLOWORLD
///////////////////////////////

const iotaLibrary = require('@iota/core')

const iota = iotaLibrary.composeAPI({
  provider: 'http://etodevice:EtoGruppe@26062020@88.79.243.113:14265'
})
// const depth = 3;
// const minimumWeightMagnitude = 9;
const address =
  'RBTSG9BZBXYXQKAANNVRMSCICNI9FHRBHDHNSBJOUHWLJFARIQQGONYGWGHXYJDCBTKT9MUJDOYOEYLTD'
const seed =
  'QPQEYBOMQZUFZSCXNPSPGGBMGGXRFAQ9WESGDZSDSLOH9TBBZIIEOYJUKTMTAJZIPBWFWCRWUHGFIAWWT'

const converter = require('@iota/converter')
const datamessage={
  "sensortype":"UV sensor",
  "data":[
    20,
    30,
    40
  ],
  "timestamp":new Date()
}
const message=JSON.stringify(datamessage);
// const message = JSON.stringify({"message": "Hello world"})

const message_in_trytes = converter.asciiToTrytes(message)

const transfers = [
  {
    value: 0,
    address: address,
    message: message_in_trytes
  }
]

iota
  .prepareTransfers(seed, transfers)
  .then(trytes => iota.sendTrytes(trytes, (depth = 3), (mwm = 14)))
  .then(bundle => {
    console.log(bundle)
  })
  .catch(err => {
    console.error(err)
  })



// iota.prepareTransfers(seed, transfers)
//     .then(trytes => {
//         return iota.sendTrytes(trytes, depth, minimumWeightMagnitude);
//     })
//     .then(bundle => {
//         console.log(bundle[0])
//     })
//     .catch(err => {
//         console.error(err)
//     });
